package com.example.suggest_movie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SuggestMovieApplication {

    public static void main(String[] args) {
        SpringApplication.run(SuggestMovieApplication.class, args);
    }

}
